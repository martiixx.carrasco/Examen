
package accesodatos;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import negocio.Producto;
import servicio.Conexion;


public class ProductoDAO extends Conexion{
    private Producto p;

    public ProductoDAO() {
    }

    public ProductoDAO(Producto p) {
        this.p = p;
    }
    
    /**
     * Método que usted debe implementar
     */
    public boolean grabar(String nombre,String moneda,double valor){
        String query="INSERT INTO examen.producto (pr_nombre, pr_moneda,pr_valormoneda) values ('"+nombre+"','"+moneda+"','"+valor+"'); ";
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement(query);
            pstm.execute();
            pstm.close();
            JOptionPane.showMessageDialog(null, "Producto agregado a la base de datos");
            return true;
        }catch(SQLException e){
            System.err.println( e.getMessage() );
        }
        return false;
        
    }
}
