package negocio;

import java.io.IOException;
import presentacion.Vista;
import servicio.Conexion;


public class Main {
     public static void main(String[] args) throws IOException {
         Conexion.getConexion();
         new controlador(new Vista() ).iniciar();
     }
}
