package negocio;

import accesodatos.ProductoDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import presentacion.Vista;
import servicio.Conexion;
import negocio.Producto;





public class controlador implements ActionListener {
    private Vista principal;
    private ProductoDAO prodcto2 = new ProductoDAO();
    private Producto producto = new Producto();
    String indicador;
    URL url;
    private double valor;


    public enum Accion{
        btnagregar;
        
    }
    
    public controlador( JFrame padre ){
        this.principal = (Vista) padre;
    }
    
    public void iniciar() throws MalformedURLException, IOException{
        try{
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            SwingUtilities.updateComponentTreeUI(principal);
            this.principal.setLocationRelativeTo(null);
            this.principal.setTitle("Agregar Productos");
            this.principal.setVisible(true);
            this.principal.radiodolar.setSelected(true);
            
            
        }catch(UnsupportedLookAndFeelException ex){}
         
         catch (ClassNotFoundException ex) {}
         catch (InstantiationException ex) {}
         catch (IllegalAccessException ex) {}    
        
        
        
        this.principal.btnagregar.setActionCommand("btnagregar");
        this.principal.btnagregar.addActionListener(this);
        
        
        
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch ( Accion.valueOf( e.getActionCommand() ) )
        {
            case btnagregar: 
                this.principal.buttonGroup1.getSelection();
                try{
                    if(this.principal.radiodolar.isSelected()){
                    indicador = "dolar";
                }
                    if(this.principal.radioeuro.isSelected()){
                    indicador = "euro";
                }
                    this.principal.txtvalor.setText(String.valueOf(valor));
                    url = new URL("http://mindicador.cl/api"+  "/"+ indicador);
                    InputStream entrada = url.openStream();
                    JsonReader reader = Json.createReader(entrada);
                    JsonObject objeto= reader.readObject();
                    //System.out.println("valor"+(i+1)+" : "+ obj.getJsonArray("serie").getJsonObject(i).get("valor"));
                    valor = Double.parseDouble(objeto.getJsonArray("serie").getJsonObject(0).get("valor").toString());
                    JOptionPane.showMessageDialog(null, valor);
                    System.out.println(valor);
                }catch(MalformedURLException ex){}
                catch(IOException ex){}
                
                
                
                
                String nombre = this.principal.txtnombre.getText();
                this.prodcto2.grabar(nombre, indicador, valor);                
                this.principal.txtvalor.setText(String.valueOf(valor));
                
                break;
    }
}
}
